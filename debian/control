Source: etsf-io
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Damien Caliste <damien.caliste@cea.fr>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               gfortran,
               libnetcdf-dev (>= 1:4.3.3.1),
               libnetcdff-dev
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/science-team/etsf-io
Vcs-Git: https://salsa.debian.org/science-team/etsf-io.git
Homepage: https://github.com/ElectronicStructureLibrary/libetsf_io

Package: etsf-io
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Binary tools to check, merge and read ETSF files
 The European Theoretical Spectroscopy Facility (ETSF) is a European
 network dedicated to providing support and services for ongoing
 research in academic, government and industrial laboratories.
 .
 The ETSF is divided into 7 beamlines, each of which is concerned with
 a specific scientific topic:
  - Optics ;
  - Energy Loss Spectroscopy ;
  - Quantum Transport ;
  - Time-resolved Spectroscopy ;
  - Photo-emission Spectroscopy ;
  - Vibrational Spectroscopy ;
  - X-Rays Spectroscopy.
 .
 To allow the adoption of its recommendations about standardization, the
 ETSF proposes different libraries and tools implementing or using these
 specifications, as well as widely usable pieces of software.
 .
 ETSF_IO is a library of F90 routines to read/write the ETSF file format.
 This package contains the user tools to:
  - check file conformance to the specifications;
  - extract data from files;
  - merge multiple files from parallel runs, as specified in the
    specifications.

Package: libetsf-io-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Breaks: libetsf-io-dev (<< 1.0.4-2)
Replaces: libetsf-io-dev (<< 1.0.4-2)
Multi-Arch: foreign
Description: Developer documentation API and tutorials for ETSF_IO
 ETSF_IO is a library of F90 routines to read/write the ETSF file format.
 .
 This Package contains the HTML documentation of the API and some tutorials
 on how to use the library in electronic structure codes.

Package: libetsf-io-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libnetcdf-dev,
         libnetcdff-dev
Suggests: libetsf-io-doc
Description: Static libraries and Fortran module files of ETSF_IO
 ETSF_IO is a library of F90 routines to read/write the ETSF file format.
 .
 This package contains the static libraries provided by ETSF_IO to let
 electronic structure codes read and write ETSF files. It also contains
 the module file used by the Fortran compiler.
