!{\src2tex{textfont=tt}}
!!****m* etsf_kpoints/etsf_io_kpoints_copy
!! NAME
!!  etsf_io_kpoints_copy
!!
!! FUNCTION
!!    This routine copy all variable of a group from one file @ncid_from to another
!!    @ncid_to. If a variable is missing in the source file, this does not raise an
!!    error, it is simply skipped. But if a variable in the destination file is not
!!    defined, this will raise an error.
!!
!!    The copy is done per variable. This means that memory occupation is reduced
!!    during the copy.
!!
!!    Normally, copies are pristine copies. But if optional argument @split is
!!    given, then the read values are copied to the specified locations in split
!!    arrays. In that case, the destination variable must have a compatible definition
!!    with the split values.
!!
!! COPYRIGHT
!!  Copyright (C) 2006-2010 (Damien Caliste)
!!  This file is distributed under the terms of the
!!  GNU Lesser General Public License, see the COPYING file
!!  or http://www.gnu.org/copyleft/lesser.txt .
!!
!! INPUTS
!! * ncid_to = 
!!     integer returned by an 'open' NetCDF call. This id must have write access
!!     granted. It will be modified by the routine. The file must be in write
!!     mode (see etsf_io_low_set_write_mode()).
!! * ncid_from = 
!!     integer returned by an 'open' NetCDF call. This id must have read access
!!     granted. It will be left untouched.
!! * dims <type(etsf_dims)> = 
!!     these dimensions correspond to the source_file ones and are used to allocate
!!     temporary arrays in memory during the copy.
!! * split <type(etsf_split)> = (optional) 
!!     if this argument is given, the values in the split definition (e.g. my_kpoints)
!!     are used to put the data in the destination file in a bigger array at the right
!!     placed.
!! OUTPUT
!! * lstat = 
!!     return .true. if all the actions succeed, if not the status
!!     of the file is undefined.
!! * error_data <type(etsf_io_low_error)> = 
!!     contains the details of the error is @lstat is false.
!!
!! NOTES
!!  This file has been automatically generated by the autogen_subroutines.py
!!  script. Any change you would bring to it will systematically be
!!  overwritten.
!!
!! SOURCE
subroutine etsf_io_kpoints_copy(ncid_to, ncid_from, dims, lstat, error_data, &
  & split)

  !Arguments ------------------------------------
  integer, intent(in) :: ncid_to
  integer, intent(in) :: ncid_from
  type(etsf_dims), intent(in) :: dims
  logical, intent(out) :: lstat
  type(etsf_io_low_error), intent(out) :: error_data
  type(etsf_split), optional, intent(in) :: split

  !Local variables-------------------------------
  character(len = *), parameter :: my_name = 'etsf_io_kpoints_copy'
  type(etsf_split) :: my_split
  integer,allocatable :: varids(:,:)
  integer :: nvarids
  integer,allocatable :: start(:)
  integer,allocatable :: count(:)
  integer :: len
  integer :: istart
  integer :: idim1,idim2,idim3,idim4,idim5,idim6,idim7,idim8
  integer,allocatable :: istop(:)
  integer,allocatable :: jstart(:)
  integer,allocatable :: jend(:)
  type(etsf_kpoints) :: folder


  ! *************************************************************************

!DEBUG
!write (*,*) 'etsf_io_kpoints_copy : enter'
!ENDDEBUG

  lstat = .false.
  call etsf_io_low_set_write_mode(ncid_to, lstat, error_data = error_data)
  if (.not. lstat) return
  
  allocate(varids(2,5))
  nvarids = 1
  
  ! Variable 'kpoint_grid_shift'
  !  allocate and read data
  allocate(folder%kpoint_grid_shift( &
    & dims%number_of_reduced_dimensions))
  call etsf_io_low_read_var(ncid_from, "kpoint_grid_shift", &
                          & folder%kpoint_grid_shift, lstat, &
                          & error_data = error_data, ncvarid = varids(1, nvarids))
  if (.not. lstat .and. error_data%access_mode_id /= ERROR_MODE_INQ) then
    deallocate(folder%kpoint_grid_shift)
    deallocate(varids)
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  !  write data and deallocate (if read succeed)
  if (lstat) then
    call etsf_io_low_write_var(ncid_to, "kpoint_grid_shift", &
                             & folder%kpoint_grid_shift, lstat, &
                             & error_data = error_data, ncvarid = varids(2, nvarids))
    if (.not. lstat) then
      deallocate(folder%kpoint_grid_shift)
      deallocate(varids)
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
    nvarids = nvarids + 1
  end if
  deallocate(folder%kpoint_grid_shift)
  
  lstat = .true.
  ! Variable 'kpoint_grid_vectors'
  !  allocate and read data
  allocate(folder%kpoint_grid_vectors( &
    & dims%number_of_vectors, &
    & dims%number_of_reduced_dimensions))
  call etsf_io_low_read_var(ncid_from, "kpoint_grid_vectors", &
                          & folder%kpoint_grid_vectors, lstat, &
                          & error_data = error_data, ncvarid = varids(1, nvarids))
  if (.not. lstat .and. error_data%access_mode_id /= ERROR_MODE_INQ) then
    deallocate(folder%kpoint_grid_vectors)
    deallocate(varids)
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  !  write data and deallocate (if read succeed)
  if (lstat) then
    call etsf_io_low_write_var(ncid_to, "kpoint_grid_vectors", &
                             & folder%kpoint_grid_vectors, lstat, &
                             & error_data = error_data, ncvarid = varids(2, nvarids))
    if (.not. lstat) then
      deallocate(folder%kpoint_grid_vectors)
      deallocate(varids)
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
    nvarids = nvarids + 1
  end if
  deallocate(folder%kpoint_grid_vectors)
  
  lstat = .true.
  ! Variable 'monkhorst_pack_folding'
  !  allocate and read data
  allocate(folder%monkhorst_pack_folding( &
    & dims%number_of_vectors))
  call etsf_io_low_read_var(ncid_from, "monkhorst_pack_folding", &
                          & folder%monkhorst_pack_folding, lstat, &
                          & error_data = error_data, ncvarid = varids(1, nvarids))
  if (.not. lstat .and. error_data%access_mode_id /= ERROR_MODE_INQ) then
    deallocate(folder%monkhorst_pack_folding)
    deallocate(varids)
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  !  write data and deallocate (if read succeed)
  if (lstat) then
    call etsf_io_low_write_var(ncid_to, "monkhorst_pack_folding", &
                             & folder%monkhorst_pack_folding, lstat, &
                             & error_data = error_data, ncvarid = varids(2, nvarids))
    if (.not. lstat) then
      deallocate(folder%monkhorst_pack_folding)
      deallocate(varids)
      call etsf_io_low_error_update(error_data, my_name)
      return
    end if
    nvarids = nvarids + 1
  end if
  deallocate(folder%monkhorst_pack_folding)
  
  lstat = .true.
  ! Variable 'reduced_coordinates_of_kpoints'
  !  allocate and read data
  allocate(folder%reduced_coordinates_of_kpoints( &
    & dims%my_number_of_kpoints, &
    & dims%number_of_reduced_dimensions))
  call etsf_io_low_read_var(ncid_from, "reduced_coordinates_of_kpoints", &
                          & folder%reduced_coordinates_of_kpoints, lstat, &
                          & error_data = error_data, ncvarid = varids(1, nvarids))
  if (.not. lstat .and. error_data%access_mode_id /= ERROR_MODE_INQ) then
    deallocate(folder%reduced_coordinates_of_kpoints)
    deallocate(varids)
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  !  write data and deallocate (if read succeed)
  if (lstat) then
    if (present(split)) then
      ! We use the split definition to write to appropriated locations.
      allocate(start(2), count(2))
      count(:) = 0
      start(:) = 1
      ! For each dimension, set the do loop boundaries,
      ! and the array boundaries.
      allocate(istop(2))
      allocate(jstart(2), jend(2))
      if (.not. associated(split%my_kpoints)) then
        istop(2)  = 1
        jstart(2) = 1
        jend(2)   = dims%my_number_of_kpoints
      else
        istop(2) = size(split%my_kpoints)
        count(2) = 1
      end if
      do idim2 = 1, istop(2), 1
        if (associated(split%my_kpoints)) then
          start(2)  = split%my_kpoints(idim2)
          jstart(2) = split%my_kpoints(idim2)
          jend(2)   = split%my_kpoints(idim2)
        end if
        call etsf_io_low_write_var(ncid_to, "reduced_coordinates_of_kpoints", &
                                 & folder%reduced_coordinates_of_kpoints(:, jstart(2):jend(2)), &
                                 & lstat, error_data = error_data, &
                                 & start = start, count = count, ncvarid = varids(2, nvarids))
        if (.not. lstat) then
          deallocate(folder%reduced_coordinates_of_kpoints)
          deallocate(start, count, istop)
          deallocate(jstart, jend)
          deallocate(varids)
          call etsf_io_low_error_update(error_data, my_name)
          return
        end if
      end do
      deallocate(start, count, istop)
      deallocate(jstart, jend)
    else
      ! No split information, we copy everything in the same shape.
      call etsf_io_low_write_var(ncid_to, "reduced_coordinates_of_kpoints", &
                               & folder%reduced_coordinates_of_kpoints, lstat, &
                               & error_data = error_data, ncvarid = varids(2, nvarids))
      if (.not. lstat) then
        deallocate(folder%reduced_coordinates_of_kpoints)
        deallocate(varids)
        call etsf_io_low_error_update(error_data, my_name)
        return
      end if
    end if
    nvarids = nvarids + 1
  end if
  deallocate(folder%reduced_coordinates_of_kpoints)
  
  lstat = .true.
  ! Variable 'kpoint_weights'
  !  allocate and read data
  allocate(folder%kpoint_weights( &
    & dims%my_number_of_kpoints))
  call etsf_io_low_read_var(ncid_from, "kpoint_weights", &
                          & folder%kpoint_weights, lstat, &
                          & error_data = error_data, ncvarid = varids(1, nvarids))
  if (.not. lstat .and. error_data%access_mode_id /= ERROR_MODE_INQ) then
    deallocate(folder%kpoint_weights)
    deallocate(varids)
    call etsf_io_low_error_update(error_data, my_name)
    return
  end if
  !  write data and deallocate (if read succeed)
  if (lstat) then
    if (present(split)) then
      ! We use the split definition to write to appropriated locations.
      allocate(start(1), count(1))
      count(:) = 0
      start(:) = 1
      ! For each dimension, set the do loop boundaries,
      ! and the array boundaries.
      allocate(istop(1))
      allocate(jstart(1), jend(1))
      if (.not. associated(split%my_kpoints)) then
        istop(1)  = 1
        jstart(1) = 1
        jend(1)   = dims%my_number_of_kpoints
      else
        istop(1) = size(split%my_kpoints)
        count(1) = 1
      end if
      do idim1 = 1, istop(1), 1
        if (associated(split%my_kpoints)) then
          start(1)  = split%my_kpoints(idim1)
          jstart(1) = split%my_kpoints(idim1)
          jend(1)   = split%my_kpoints(idim1)
        end if
        call etsf_io_low_write_var(ncid_to, "kpoint_weights", &
                                 & folder%kpoint_weights(jstart(1):jend(1)), &
                                 & lstat, error_data = error_data, &
                                 & start = start, count = count, ncvarid = varids(2, nvarids))
        if (.not. lstat) then
          deallocate(folder%kpoint_weights)
          deallocate(start, count, istop)
          deallocate(jstart, jend)
          deallocate(varids)
          call etsf_io_low_error_update(error_data, my_name)
          return
        end if
      end do
      deallocate(start, count, istop)
      deallocate(jstart, jend)
    else
      ! No split information, we copy everything in the same shape.
      call etsf_io_low_write_var(ncid_to, "kpoint_weights", &
                               & folder%kpoint_weights, lstat, &
                               & error_data = error_data, ncvarid = varids(2, nvarids))
      if (.not. lstat) then
        deallocate(folder%kpoint_weights)
        deallocate(varids)
        call etsf_io_low_error_update(error_data, my_name)
        return
      end if
    end if
    nvarids = nvarids + 1
  end if
  deallocate(folder%kpoint_weights)
  
  lstat = .true.
  
  ! We copy all the attributes (ETSF and non-ETSF) of the group variables.
  call etsf_io_low_set_define_mode(ncid_to, lstat, error_data = error_data)
  if (.not. lstat) nvarids = 0
  do len = 1, nvarids - 1, 1
    call etsf_io_low_copy_all_att(ncid_from, ncid_to, varids(1, len), varids(2, len), &
                                & lstat, error_data = error_data)
    if (.not. lstat) then
      call etsf_io_low_error_update(error_data, my_name)
      exit
    end if
  end do
  deallocate(varids)

!DEBUG
!write (*,*) 'etsf_io_kpoints_copy : exit'
!ENDDEBUG

end subroutine etsf_io_kpoints_copy
!!***
